﻿open AlchemyF.Commands.AuthCommand
open AlchemyF.Commands.RegisterKillCommand
open Spectre.Console.Cli
open Alchemy.Commands.TimersCommand
open System

let app = CommandApp()

app.Configure (fun cfg ->
    cfg.AddCommand<TimersCommand>("timers") |> ignore
    cfg.AddCommand<AuthCommand>("auth") |> ignore
    cfg.AddCommand<RegisterKillCommand>("kill") |> ignore)

let args =
    Environment.GetCommandLineArgs()
    |> Array.filter (fun x -> x.Contains(".dll") = false)

app.Run(args) |> ignore

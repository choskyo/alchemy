module AlchemyF.Utils.MapResponses

// Map anet/user-input names from /worldbosses to the names in the csv
let WorldBosses (anetBosses: seq<string>) =
    anetBosses |> Seq.map (fun x ->
        match x.ToLower().Trim() with
            | "admiral_taidha_covington" | "adm" | "taidha" -> "Admiral Taidha"
            | "claw_of_jormag" | "claw" | "jormag" -> "Claw of Jormag"
            | "drakkar" | "dkr" -> "Drakkar"
            | "fire_elemental" | "ele" -> "Fire Elemental"
            | "great_jungle_wurm" | "wurm" | "gjw" -> "Great Jungle Wurm"
            | "inquest_golem_mark_ii" | "golem" | "mk2" -> "Golem MK II"
            | "karka_queen" | "karka" -> "Karka Queen"
            | "megadestroyer" | "mega" | "destroyer" -> "Megadestroyer"
            | "svanir_shaman_chief" | "svanir" | "chief" -> "Svanir Chief"
            | "tequatl_the_sunless" | "tequatl" | "teq" -> "Tequatl the Sunless"
            | "ds" | "storm" | "dstorm" -> "Dragonstorm"
            | "octo" | "tarir" -> "Octovine"
            | "chak" | "gerant" -> "Chak Gerant"
            | "stand" | "dstand" | "mor" -> "Dragon's Stand"
            | "pta" -> "Pinata"
            | "pto" -> "Path to Ascension"
            | "de" | "end" -> "Dragon's End (Pre)"
            | _ -> failwith $"Unexpected name: {x}"
        )
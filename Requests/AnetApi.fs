module AlchemyF.Requests.AnetApi

open System.Net.Http
open System.Text.Json
open AlchemyF
open AlchemyF.Utils

let baseUrl = "https://api.guildwars2.com/v2"

let authenticatedGet endpoint =
    let cfg = Configuration.Get
    
    use httpClient = new HttpClient()
    httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {cfg.ApiKey}")
        
    httpClient.GetStringAsync $"{baseUrl}{endpoint}" |> Async.AwaitTask |> Async.RunSynchronously

let WorldBossesDefeated =
    authenticatedGet "/account/worldbosses"
    |> JsonSerializer.Deserialize<seq<string>>
    |> MapResponses.WorldBosses
    
module AlchemyF.Configuration

open System
open System.IO
open System.Text.Json

type Configuration = {
    ApiKey: string
    TimerKilledEmoji: string
}

let DefaultConfiguration = {
    ApiKey = ""
    TimerKilledEmoji = "check_mark"
}

let CfgPaths =
    let cfgDirPath =
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "alchemy")
    
    let cfgFilePath = Path.Combine(cfgDirPath, "cfg.json")
    
    (cfgDirPath, cfgFilePath)

let Get =
    let cfgDirPath, cfgFilePath = CfgPaths
    
    Directory.CreateDirectory(cfgDirPath) |> ignore

    if not (File.Exists cfgFilePath) then
        File.WriteAllText(cfgFilePath, JsonSerializer.Serialize(DefaultConfiguration))

    let cfg = File.ReadAllText cfgFilePath |> JsonSerializer.Deserialize<Configuration>

    cfg

let Set newCfg =
    let _, cfgFilePath = CfgPaths
    
    File.Delete cfgFilePath
    File.WriteAllText(cfgFilePath, JsonSerializer.Serialize(newCfg))
module Data.EventTimes

open EventData
open System
open FSharp.Data

let parseStart (time: string) =
    let segs = time.Split [|':'|]
    let now = DateTime.UtcNow
    DateTime(now.Year, now.Month, now.Day, int segs[0], int segs[1], 0)

let private loadData fileName =
    CsvFile.Load($"data/{fileName}.csv").Rows
    |> Seq.map (fun x ->
        { Name = x.Columns[0]
          StartsAt = parseStart x.Columns[1]
          Waypoint = x.Columns[2]
          Frequency = Frequency.Parse(x.Columns[3]) })

let Core = loadData "core"
let Hot = loadData "hot"
let Pof = loadData "pof"
let Ibs = loadData "ibs"
let Eod = loadData "eod"

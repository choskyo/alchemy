module Data.EventData

open System

type Frequency =
    | Hourly = 1
    | EveryOtherHour = 2
    | EveryThreeHours = 3
    | EveryFourHours = 4

type EventData =
    { Name: string
      StartsAt: DateTime
      Waypoint: string
      Frequency: Frequency }

let EventAllStarts (e: EventData) : seq<DateTime> =
    e.StartsAt
    |> Seq.unfold (fun start ->
        if start > DateTime.UtcNow.AddHours 12 then
            None
        else
            Some(start, start.AddHours (int e.Frequency)))

let EventNextStart (e: EventData) =
    EventAllStarts e
    |> Seq.skipWhile (fun x -> x.AddMinutes(5) < DateTime.UtcNow)
    |> Seq.head

module Alchemy.Commands.TimersCommand

open AlchemyF
open AlchemyF.Commands.RegisterKillCommand
open AlchemyF.Requests
open Spectre.Console.Cli
open Data.EventData
open Spectre.Console
open System

type TimersCommand() =
    inherit Command()

    let printTable (tableName: string) (events: seq<EventData>) (bossesDefeated: seq<string>) =
        let cfg = Configuration.Get

        AnsiConsole.MarkupLineInterpolated($"[bold]{tableName}[/]")
        let table = Table()

        table.AddColumns(TableColumn("Name"), TableColumn("Next Start"), TableColumn("Waypoint"))
        |> ignore

        for e in events |> Seq.sortBy EventNextStart do
            let nextStart = EventNextStart e

            let now = DateTime.UtcNow

            let colour =
                match nextStart with
                | _ when now > nextStart || now.AddMinutes(5) > nextStart -> "green"
                | _ when now.AddMinutes(60) > nextStart -> "white"
                | _ -> "grey"

            let defeated =
                match bossesDefeated |> Seq.contains e.Name with
                | true -> $" :{cfg.TimerKilledEmoji}:"
                | _ -> ""

            let txtFmt = $"{colour}"

            table.AddRow(
                Markup($"[{txtFmt}]{e.Name}{defeated}[/]"),
                Markup($"[{txtFmt}]{nextStart.ToShortTimeString()}[/]"),
                Markup($"[{txtFmt}]{e.Waypoint}[/]")
            )
            |> ignore

        AnsiConsole.Write(table)

        0

    override this.Execute(context: CommandContext) : int =
        let apiBosses = AnetApi.WorldBossesDefeated

        let manualBosses =
            RegisterKillCommand.GetCache

        let bosses =
            Seq.concat [ apiBosses; manualBosses ]

        context.Remaining.Parsed
        |> Seq.map (fun x ->
            match x.Key with
            | "core" -> printTable "Mainland Tyria" Data.EventTimes.Core bosses
            | "hot" -> printTable "Maguuma Jungle" Data.EventTimes.Hot bosses
            | "pof" -> printTable "Crystal Desert" Data.EventTimes.Pof bosses
            | "ibs" -> printTable "Eye of the North" Data.EventTimes.Ibs bosses
            | "eod" -> printTable "Seitung Province" Data.EventTimes.Eod bosses
            | _ -> 1)
        |> Seq.sum

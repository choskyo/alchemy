module AlchemyF.Commands.AuthCommand

open System
open AlchemyF
open Spectre.Console.Cli

type AuthCommand() =
    inherit Command()

    override this.Execute(context) =
        let cfg = Configuration.Get

        let apiKey = context.Remaining.Parsed.Item "key" |> Seq.head

        Configuration.Set { cfg with ApiKey = apiKey  }

        0

module AlchemyF.Commands.RegisterKillCommand

open System
open System.IO
open System.Text.Json
open AlchemyF
open AlchemyF.Utils
open Spectre.Console.Cli

type Settings() =
    inherit CommandSettings()

    [<CommandArgument(0, "[Names]")>]
    member val Names = Array.empty<string> with get, set


type RegisterKillCommand() =
    inherit Command<Settings>()

    static member GetCachePath =
        let cfgDir, _ = Configuration.CfgPaths
        let dateFmt = "yyyy-MM-dd"
        Path.Combine(cfgDir, $"{DateTime.UtcNow.ToString(dateFmt)}.json")

    static member GetCache =
        let path = RegisterKillCommand.GetCachePath

        if not (File.Exists path) then
            File.WriteAllText(path, JsonSerializer.Serialize(Seq.empty<string>))

        File.ReadAllText path
        |> JsonSerializer.Deserialize<seq<string>>

    static member WriteCache(kills: seq<string>) =
        let path = RegisterKillCommand.GetCachePath
        File.WriteAllText(path, (JsonSerializer.Serialize kills))

    override this.Execute(context, settings) =
        if settings.Names |> Seq.contains "clear" then
            RegisterKillCommand.WriteCache Seq.empty<string>
            0
        else
            let cache = RegisterKillCommand.GetCache

            let mappedKills =
                settings.Names |> MapResponses.WorldBosses

            Seq.append cache mappedKills
            |> RegisterKillCommand.WriteCache

            0
